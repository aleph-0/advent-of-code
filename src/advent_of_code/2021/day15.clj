(ns advent-of-code.2021.day15
  (:require [clojure.string :as s]
            [advent-of-code.utils :as utils]))


(def grid (->> (slurp "resources/2021/day15.txt")
               (s/split-lines)
               (utils/index2d)))

(defn wrap [x]
  (if (<= x 9) x (mod x 9)))

(def grid-2
  (let [m 100]
    (->> (for [x (range m)
               y (range m)
               i (range 5)
               j (range 5)]
           (let [v (grid [x y])]
             [[(+ x (* i m)) (+ y (* j m))] (wrap (+ v i j))]))
         (into {}))))


