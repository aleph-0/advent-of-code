(ns advent-of-code.core
  (:require [advent-of-code.2021.day15 :as day15])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (day15/dijkstra [0 0] [99 99])))
