(ns advent-of-code.2015.day12
  (:require [clojure.string :as s]))


(def input (-> (slurp "resources/2015/day12.txt")
               (s/replace ":" " ")))

(def json-doc (read-string input))

(defn red? [m]
  (-> (concat (keys m) (vals m))
      (.contains "red")))

(defn find-nums [coll]
  (cond
    (nil? coll) [0]
    (number?  coll) [coll]
    (sequential? coll) (mapcat find-nums coll)
    (map? coll)
    (if (red? coll)
      []
      (mapcat find-nums (vals coll)))))

(defn solve []
  (->> (find-nums json-doc)
       (reduce +)))
