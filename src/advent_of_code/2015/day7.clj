(ns advent-of-code.2015.day7
  (:require [clojure.string :as s]
            [clojure.core.match :refer [match]]))


(def input (->> (slurp "resources/2015/day7.txt")
                (s/split-lines)
                (map #(s/split % #"\s+"))))

(defn step [state instruction]
  (match (apply vector instruction)
         [v "->" k] (assoc state k v)
         [x "AND" y "->" z] (assoc state z (bit-and x y))
         [x "OR" y "->" z] (assoc state z (bit-or x y))
         :else (throw (Throwable. "invalid state"))))
