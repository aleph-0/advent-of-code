(ns advent-of-code.2015.day14
  (:require [clojure.string :as s]))

(defn parse-reindeer [s]
  (let [[a b c] (map read-string (re-seq #"\d+" s))]
    {:speed a :endurance b :rest c
     :cur-end b :cur-rest c
     :flying true :pos 0}))

(def reindeers (->> (slurp "resources/2015/day14.txt")
                    (s/split-lines)
                    (map parse-reindeer)))

(defn update-reindeer [reindeer]
  (if (reindeer :flying)
    (if (> (reindeer :cur-end) 0)
      (-> (update reindeer :pos #(+ % (reindeer :speed)))
          (update :cur-end dec))
      (-> (assoc reindeer :flying false)
          (assoc :cur-rest (reindeer :rest))))
    (if (> (reindeer :cur-rest) 0)
      (update reindeer :cur-rest dec)
      (-> (assoc reindeer :flying true)
          (assoc :cur-end (reindeer :endurance))))))

(defn fly-reindeer [reindeer]
  (loop [x 0 rd reindeer]
    (if (= x 2503) rd
        (recur (inc x) (update-reindeer rd)))))

(defn part-1 []
  (->> (map fly-reindeer reindeers)
       (apply max-key :pos)))
