(ns advent-of-code.2015.day13
  (:require [clojure.string :as s]
            [clojure.math.combinatorics :as combi]))


(defn parse [sentence]
  (let [xs (s/split sentence #"\s+")
        hd (first xs)
        tl (s/replace (last xs) "." "")
        score (Integer/parseInt (re-find #"\d+" sentence))]
    [[hd tl] (if (.contains sentence "gain") score (* -1 score))]))


(def score-map (->> (slurp "resources/2015/day13.txt")
                    (s/split-lines)
                    (map parse)
                    (into {})))

(defn table-score [table]
  (let [order (conj (vec table) (first table))
        pairs (map vec (partition 2 1 order))
        rev-pairs (map reverse pairs)]
    (reduce + (map #(get score-map % 0) (concat pairs rev-pairs)))))

(defn part-1 []
  (let [candidates (set (apply concat (keys score-map)))
        perms (combi/permutations (conj candidates "kevin"))]
    (apply max (map table-score perms))))
